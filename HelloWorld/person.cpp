#include "person.h"

#include <string>

Person::Person(const std::string &name)
{
  setName(name);
}

const std::string& Person::getName() const
{
  return name;
}

void Person::setName(const std::string &value)
{
  name = value;
}
