#include "person.h"

#include <iostream>

/**
 * @brief main My main function
 * @return Exit status code
 */
int main()
{
  std::cout << "Hello World!" << std::endl;

  Person me{"James Bond"};

  std::cout << "Welcome " << me.getName() << " to this example C++ project." << std::endl;

  return 0;
}
