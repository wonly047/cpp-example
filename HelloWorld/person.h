#ifndef PERSON_H
#define PERSON_H

#include <string>

class Person
{
public:
  Person(const std::string &name);
  ~Person() = default;

  std::string name;

  const std::string& getName() const;
  void setName(const std::string &value);
};

#endif // PERSON_H
